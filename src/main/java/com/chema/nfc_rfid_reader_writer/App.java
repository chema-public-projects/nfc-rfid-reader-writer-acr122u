package com.chema.nfc_rfid_reader_writer;

import javax.smartcardio.CardException;

import com.chema.nfc_rfid_reader_writer.rfid.ACR;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
      System.out.println( "Hello World!" );
      try {
        // Test the smartcardio 
				ACR.getDefaultCardTerminal().waitForCardPresent(0);
			} catch (CardException e) {
				e.printStackTrace();
			}
    }
}
