package com.chema.nfc_rfid_reader_writer.rfid;

import java.util.Arrays;
import javax.smartcardio.*;

public class RFID {
    
    static byte[] ApduGetCardUID = {
      (byte) 0xff,
      (byte) 0xca,
      (byte) 0x00,
      (byte) 0x00,
      (byte) 0x00
    };
    
    public static void main(String[] arg)throws CardException {
        try{
            //byte[] APDUUpdate = APDU.GetApduWriteTag(2, "Jesek Läckberg");
            
            //System.out.println(ACR.getTagUID());
            //System.out.println(ACR.loadDefaultAuthenticationKeyToReader());
            //System.out.println(ACR.authenticateTagBlockDefaultKey(2));
            //System.out.println(ACR.readTagBlock(2));
            
            //System.out.println(ACR.readTagBlock(2));
            //System.out.println(ACR.readTagBlock(4));
            System.out.println("=== Escritura ==");
            System.out.println("Coloque la tarjeta en el lector y no la retire hasta que se le indique...");
            ACR.getDefaultCardTerminal().waitForCardPresent(0);
            ACR.writeTagBlockWithDefaults(1, "Hola Charly Elea");
//            ACR.writeTagBlockWithDefaults(2, "Prueba");
//            ACR.writeTagBlockWithDefaults(4, "Mariano");
//            ACR.writeTagBlockWithDefaults(5, "Méndez V.");
            System.out.println("Puede retirar la tarjeta");
            ACR.getDefaultCardTerminal().waitForCardAbsent(0);
            System.out.println("=================================");
            System.out.println("=== Lectura ===");
            System.out.println("Coloque la tarjeta en el lector y no la retire hasta que se le indique...");
            while(true){
                if(ACR.getDefaultCardTerminal().isCardPresent()){
                    String bloque1 = ACR.readTagBlock(1);
                    String bloque2 = ACR.readTagBlock(2);
                    String foo = "9fa896e6";
//                    String nombreCliente = ACR.readTagBlock(4);
//                    String apellidosCliente = ACR.readTagBlock(5);
//                    System.out.println("Puede retirar la tarjeta");
//                    ACR.getDefaultCardTerminal().waitForCardAbsent(0);
//                    System.out.println("=================================");
                    System.out.println("El bloque 1 contiene: " + bloque1);
                    System.out.println(foo);
                    //System.out.println(Arrays.toString(foo.getBytes("UTF16")));
                    System.out.println(Arrays.toString(foo.getBytes()));
                    //System.out.println(Arrays.toString(idCliente.getBytes("UTF16")));
                    System.out.println("");
                    System.out.println(bloque2);
//                    System.out.println(nombreCliente);
//                    System.out.println(apellidosCliente);
//                    System.out.println("=================================");
                    String var = new String(trim(bloque2.getBytes()));
                    System.out.println(Arrays.toString(var.getBytes()));
                    String vare = "";
                    if(var.equals(foo)){
                        System.out.println("Son iguales");
                    }
                    
                    ACR.getDefaultCardTerminal().waitForCardAbsent(0);
                    if(ACR.getDefaultCardTerminal().isCardPresent())
                        break;
                }
            }
            
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
        }
    }
    static byte[] trim(byte[] bytes)
    {
        int i = bytes.length - 1;
        while (i >= 0 && bytes[i] == 0)
        {
            --i;
        }
        return Arrays.copyOf(bytes, i + 1);
    }
    
}
