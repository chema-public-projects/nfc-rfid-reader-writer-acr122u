/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chema.nfc_rfid_reader_writer.rfid;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

import com.chema.nfc_rfid_reader_writer.utils.Convert;

/**
 * @author José María
 */
public class ACR {
    public static CardTerminal getDefaultCardTerminal(){
        try{
            TerminalFactory factory = TerminalFactory.getDefault();
            List<CardTerminal> terminals = factory.terminals().list();
            CardTerminal terminal = terminals.get(0);
            return terminal;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public static CardChannel getCardDefaultBasicChannel(){
        try{
            CardTerminal terminal = getDefaultCardTerminal();
            if(terminal.isCardPresent()){
                Card card = terminal.connect("*");
                CardChannel channel = card.getBasicChannel();
                return channel;
            }
            else
                return null;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public static String getTagUID(){
        try{
            CardChannel channel = ACR.getCardDefaultBasicChannel();
            if(channel != null){//La tarjeta está colocada
                CommandAPDU command = new CommandAPDU(APDU.APDU_TAG_UID);
                ResponseAPDU response = channel.transmit(command);

                if(response.getSW1() == 0x63 && response.getSW2() == 0x00)//Codigos de respuesta del lector SW1=63 y SW2=00
                    return "Respuesta negativa";
                else
                    return Convert.BinToHex(response.getData());//retorna los datos binarios convertidos a una cadena hexadecimal
            }
            else//No está colocada la tarjeta
                return "No es posible comunicarse con la tarjeta";
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public static boolean loadDefaultAuthenticationKeyToReader(){
        boolean operationState = false;
        try{
            CardChannel channel = ACR.getCardDefaultBasicChannel();
            if(channel != null){//La tarjeta está colocada
                CommandAPDU command = new CommandAPDU(APDU.getApduLoadDefaultAuthenticationKeyToReader(APDU.ACR_MEMORY_SPACE_0));
                ResponseAPDU response = channel.transmit(command);
                if(response.getSW() == 36864) //  90 00h = operación correcta
                    operationState = true;
            }
            return operationState;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return operationState;
        }
    }
    
    public static boolean loadCustomAuthenticationKeyToReader(byte[] APDULoadAuthenticationKey){
        boolean operationState = false;
        try{
            CardChannel channel = ACR.getCardDefaultBasicChannel();
            if(channel != null){//La tarjeta está colocada
                CommandAPDU command = new CommandAPDU(APDULoadAuthenticationKey);
                ResponseAPDU response = channel.transmit(command);
                if(response.getSW() == 36864) //  90 00h = operación correcta
                    operationState = true;
            }
            return operationState;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return operationState;
        }
    }
    
    public static boolean authenticateTagBlockDefaultKey(int blockToAuthenticate){
        boolean operationState = false;
        try{
            if(loadDefaultAuthenticationKeyToReader()){
                CardChannel channel = ACR.getCardDefaultBasicChannel();
                if(channel != null){//La tarjeta está colocada
                    CommandAPDU command = new CommandAPDU(APDU.getApduCardAuthentication(blockToAuthenticate, APDU.TAG_KEY_TYPE_A, APDU.ACR_MEMORY_SPACE_0));
                    ResponseAPDU response = channel.transmit(command);
                    
                    if(response.getSW() == 36864) //  90 00h = operación correcta
                        operationState = true;
                }
            }
            return operationState;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return operationState;
        }
    }
    
    /**
     * Description: Autentica un bloque de la tarjeta con un APDU de autenticación
     * @param blockToAuthenticate
     * @return 
     */
    public static boolean authenticateTagBlock(int blockToAuthenticate){
        boolean operationState = false;
        try{
            if(loadDefaultAuthenticationKeyToReader()){
                CardChannel channel = ACR.getCardDefaultBasicChannel();
                if(channel != null){//La tarjeta está colocada
                    CommandAPDU command = new CommandAPDU(APDU.getApduCardAuthentication(blockToAuthenticate, APDU.TAG_KEY_TYPE_A, APDU.ACR_MEMORY_SPACE_0));
                    ResponseAPDU response = channel.transmit(command);
                    
                    if(response.getSW() == 36864) //  90 00h = operación correcta
                        operationState = true;
                }
            }
            return operationState;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return operationState;
        }
    }
    
    public static String readTagBlock(int BlockToRead){
        String readBlock = "Se ha producido un error durante la lectura";
        try{
            if(authenticateTagBlockDefaultKey(BlockToRead)){
                CardChannel channel = ACR.getCardDefaultBasicChannel();
                if(channel != null){//La tarjeta está colocada
                    CommandAPDU command = new CommandAPDU(APDU.getApduReadTag(BlockToRead));
                    ResponseAPDU response = channel.transmit(command);
                    
                    if(response.getSW() == 36864){ //  90 00h = operación correcta
                        String hexaStringData = Convert.BinToHex(response.getBytes());
                        hexaStringData = hexaStringData.substring(0, hexaStringData.length() - 4);
                        byte[] hexaStringDataAsBytes = Convert.hexStringToByteArray(hexaStringData);
                        String utfData = new String(hexaStringDataAsBytes, StandardCharsets.UTF_8);
                        readBlock = utfData;
                    }   
                }
            }
            return readBlock;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return readBlock;
        }
    }
    
    public static boolean writeTagBlockWithDefaults(int blockToUpdate, String stringData){
        boolean operationState = false;
        try{
            if(authenticateTagBlockDefaultKey(blockToUpdate)){
                CardChannel channel = ACR.getCardDefaultBasicChannel();
                if(channel != null){
                    CommandAPDU command = new CommandAPDU(APDU.getApduWriteTag(blockToUpdate, stringData));
                    ResponseAPDU response = channel.transmit(command);
                    
                    if(response.getSW() == 36864)
                        operationState = true;
                }
            }
            return operationState;
        }
        catch(CardException ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            return operationState;
        }
    }
    
    public static int[] getBlocksToWrite(){
        int[] arrayBlocks = null;
        try{
            
            return arrayBlocks;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            return arrayBlocks;
        }
    }
    
    public static byte[] getDataSegmentsToWrite(){
        byte[] dataSegments = null;
        try{
            return dataSegments;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            return dataSegments;
        }
    }
    
    public static String[] writeDataOnTag(String dataToWrite, String key, byte keyLocation){
        String[] resultReport = {"RESULT","MESSAGE","CAUSE"};
        try{
            byte[] bytesKey = Convert.stringToByteArray(key);
            if(bytesKey.length == 6){
                byte[] ApduLoadCustomAuthenticationKey = APDU.getApduLoadCustomAuthenticationKeyToReader(keyLocation, bytesKey);
                if(ApduLoadCustomAuthenticationKey != null){
                    if(loadCustomAuthenticationKeyToReader(ApduLoadCustomAuthenticationKey)){
                        
                    }
                    else{
                        resultReport[0] = "ERROR_LOAD_AUTHENTICATION";
                        resultReport[1] = "Error al cargar las llave de autenticación";
                        resultReport[3] = "El lector ha retornado un código de error al cargar la llave";
                    }
                }
                else{
                    resultReport[0] = "ERROR_GET_APDU";
                    resultReport[1] = "Error al construir el APDU";
                    resultReport[3] = "Se ha producido un error al crear el array de bytes del APDU";
                }
            }
            else{
                resultReport[0] = "INVALID_KEY";
                resultReport[1] = "La longitud de la llave no es válida";
                resultReport[2] = "La longitud resultante de la conversión es diferente de 6 bytes en memoria";
            }
            return resultReport;
        }
        catch(Exception ex){
            System.out.println(Arrays.toString(ex.getStackTrace()));
            System.out.println(ex.getMessage());
            resultReport[0] = "ERROR";
            resultReport[1] = "Ha ocurrido un error";
            resultReport[2] = ex.getMessage();
            return resultReport;
        }
    }
    
    
}
