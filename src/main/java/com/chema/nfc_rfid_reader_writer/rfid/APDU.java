/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chema.nfc_rfid_reader_writer.rfid;

import java.io.UnsupportedEncodingException;
import com.chema.nfc_rfid_reader_writer.utils.Convert;

/**
 *
 * @author José María
 */
public class APDU {
    
    public static byte ACR_MEMORY_SPACE_0 = (byte)0x00;
    public static byte ACR_MEMORY_SPACE_1 = (byte)0x01;
    public static byte TAG_KEY_TYPE_A = (byte)0x60;
    public static byte TAG_KEY_TYPE_B = (byte)0x61;
    
    public static byte[] APDU_TAG_UID = {
        (byte) 0xff, (byte) 0xca, 
        (byte) 0x00, (byte) 0x00, 
        (byte) 0x00
    };
    public static byte[] APDU_TAG_ISO = {
        (byte) 0xff, (byte) 0xca, 
        (byte) 0x01, (byte) 0x00, 
        (byte) 0x00
    };
    
    public static byte[] DEFAULT_KEY_AUTHENTICATION = {
        (byte) 0xFF, (byte) 0xFF, 
        (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF
    };
    
    public static byte[] getApduLoadDefaultAuthenticationKeyToReader(byte memorySpace){
        try{
            byte[] APDULoadAuthenticationKeys= {
                (byte) 0xFF, //Class
                (byte) 0x82, //INS
                (byte) 0x00, //P1 KEY STRUCTURE 00h = Key is loaded into the reader volatile memory
                memorySpace, //P2 KEY NUMBER 00h ~ 01h = Key Location. The keys will disappear once the reader is disconnected from the PC.  
                (byte) 0x06 //LC  
            };
            APDULoadAuthenticationKeys = Convert.concatenateByteArrays(APDULoadAuthenticationKeys, DEFAULT_KEY_AUTHENTICATION);
            return APDULoadAuthenticationKeys;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public static byte[] getApduLoadCustomAuthenticationKeyToReader(byte memorySpace, byte[] key){
        byte[] APDULoadAuthenticationKey = null;
        try{
            APDULoadAuthenticationKey = new byte[]{
                (byte) 0xFF, //Class
                (byte) 0x82, //INS
                (byte) 0x00, //P1 KEY STRUCTURE 00h = Key is loaded into the reader volatile memory
                memorySpace, //P2 KEY NUMBER 00h ~ 01h = Key Location. The keys will disappear once the reader is disconnected from the PC.  
                (byte) 0x06 //LC  
            };
            APDULoadAuthenticationKey = Convert.concatenateByteArrays(APDULoadAuthenticationKey, key);
            return APDULoadAuthenticationKey;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            return APDULoadAuthenticationKey;
        }
    }
    
    public static byte[] getApduCardAuthentication(int blockToAuthenticate, byte keyType, byte keyLocationOnReader){
        try{
            byte hexBlock = Convert.IntDecimalToByteHexadecimal(blockToAuthenticate);
            byte[] ApduBlockAuthentication = {
                (byte) 0xff,//Class
                (byte) 0x86,//INS
                (byte) 0x00,//P1
                (byte) 0x00,//P2
                (byte) 0x05,//LC
                //Authentication Data bytes (5bytes) | Segun el manual del acr esta parte es el data in del completeAPDU de autenticacion
                (byte) 0x01, //Byte1 Version 
                (byte) 0x00, // Byte 2 00h
                hexBlock,// Byte 3 BLOCK NUMBER This is the memory block to be authenticated
                keyType,// KEY TYPE 60h = A, 61h = B 
                keyLocationOnReader //KEY NUMBER 00h-01h  Key location (busca la llave en 00 o en 01)
            };
            return ApduBlockAuthentication;
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public static byte[] getApduReadTag(int blockToRead){
        try{
            byte blockHexByte = Convert.IntDecimalToByteHexadecimal(blockToRead);
            byte[] ApduReadBlock = {
                (byte) 0xFF, //Class
                (byte) 0xB0,//INS
                (byte) 0x00,//P1
                blockHexByte,//P2 = Block number
                (byte) 0x10 // Le = Number of bytes to read
            };
            return ApduReadBlock;
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public static byte[] getApduWriteTag(int blockToWrite, String stringData){
        try{
            byte[] byteArrayStringDecUTF8 = stringData.getBytes("UTF8");
            
            //String myString = new String(byteArrayStringDecUTF8, "UTF8");
            byte[] hexaDataByteArray = Convert.DecimalByteArrayToHexByteArray(byteArrayStringDecUTF8);
            
            String foo = Convert.ByteToHex((byte)blockToWrite);
            byte byteBlockHexaValue = Convert.UniqueValHexStringToHexByte(foo);
            
            byte[] ApduUpdateBinaryBlock = { 
                (byte) 0xff, //Class
                (byte) 0xd6, //INS
                (byte) 0x00, //P1 00h
                byteBlockHexaValue, //P2 = Block number -> The starting block to be updated.
                (byte) 0x10 // Lc = Number of bytes to update ->16 bytes for MIFARE 1K/4K || 4 bytes for MIFARE Ultralight
            };
            
            int lengthData = hexaDataByteArray.length;
            int tempLength = 16 - lengthData;
            
            if(lengthData < 16){
                for(int i = 1; tempLength >= i; i++){
                    //byte[] spaceHexaValue = {(byte)0x20};
                    byte[] emptyValue = {(byte)0x00};
                    hexaDataByteArray = Convert.concatenateByteArrays(hexaDataByteArray, emptyValue);
                }
            }
            byte[] completeAPDU = Convert.concatenateByteArrays(ApduUpdateBinaryBlock, hexaDataByteArray);
            return completeAPDU;
        }
        catch(UnsupportedEncodingException ex){
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
