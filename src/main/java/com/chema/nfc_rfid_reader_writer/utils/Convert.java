/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chema.nfc_rfid_reader_writer.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
// import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author José María
 */
public class Convert {
    
    public static String ByteToHex(byte b) {
        int i = b & 0xFF;
        return Integer.toHexString(i);
    }
    
    public static byte UniqueValHexStringToHexByte(String hexString){
        try{
            if(hexString.length() < 2){
                hexString = "0" + hexString;
            }
            byte hexByte = (byte) (Integer.parseInt(hexString,16) & 0xff);
            return hexByte;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            return 0x32;//Espacio en UTF8
        }
    }
    
    public static byte IntDecimalToByteHexadecimal(int decimalValue){
        try{
            String hexString = Integer.toHexString(decimalValue);
            byte hexByte = Byte.parseByte(hexString,16);            
            return hexByte;
        }
        catch(Exception ex){
            return 0x00;
        }
    }
    
    public static byte[] DecimalByteArrayToHexByteArray(byte[] decArr){
        try{
            String stringHexRepresentation = "";
            for (int k = 0; k < decArr.length; k++) {
                String charHexValue = UnicodeFormatter.byteToHex(decArr[k]);
                stringHexRepresentation += charHexValue;
            }
            byte[] hexbyte = HexStringToByteArray(stringHexRepresentation);
            //byte[] hexa = DatatypeConverter.parseHexBinary(stringHexRepresentation);
            return hexbyte;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public static byte[] HexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
          int index = i * 2;
          int v = Integer.parseInt(s.substring(index, index + 2), 16);
          b[i] = (byte) v;
        }
        return b;
      }
    
    public static String BinToHex(byte[] data){
        return String.format("%0" + (data.length * 2) + "X", new BigInteger(1,data));
    }
  
    public static  byte[] concatenateByteArrays(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length]; 
        System.arraycopy(a, 0, result, 0, a.length); 
        System.arraycopy(b, 0, result, a.length, b.length); 
        return result;
    } 
    
    public static byte[] hexStringToByteArray(String hex) {
        int l = hex.length();
        byte[] data = new byte[l/2];
        for (int i = 0; i < l; i += 2) {
            data[i/2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                                 + Character.digit(hex.charAt(i+1), 16));
        }
        return data;
    }
    
    public static byte[] stringToByteArray(String stringData){
        byte[] byteArrayFromString = null;
        try{
            if(stringData != null){
                byte[] byteArrayStringDecimal = stringData.getBytes();
                byteArrayFromString = byteArrayStringDecimal;
            }
            return byteArrayFromString;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            return byteArrayFromString;
        }
    }
}
