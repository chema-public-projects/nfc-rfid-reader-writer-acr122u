# NFC-RFID Reader-Writer ACR122U

This is a simple Java library to write and read RFID chips with the ACR122U antenna

You can find an example in the `src/rfid/RFID.java` file.

Also, you should read the detailed specs in the API doc at the `docs` directory or in the official site
https://www.acs.com.hk/en/products/3/acr122u-usb-nfc-reader/

|  |  |
| ------ | ------ |
| Standard | ISO/IEC 18092 NFC, ISO 14443 Type A & B, MIFARE®, FeliCa |
| Protocol | ISO 14443-4 Compliant Card, T=CL ISO 14443-4 Compliant Card, T=CL ISO18092, NFC Tags FeliCa | 

